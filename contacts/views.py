from django.views.generic import TemplateView
from contacts.models import Contacts


class ContactsView(TemplateView):
    template_name = 'contacts/contacts_list.html'

    def get_context_data(self, **kwargs):
        qs = super(ContactsView, self).get_context_data(**kwargs)
        qs['contact'] = Contacts.objects.all().first()
        return qs


