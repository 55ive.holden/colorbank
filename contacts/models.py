from django.db import models
from gallery.models import AbstractBaseModel


class Contacts(AbstractBaseModel):
    location = models.CharField(max_length=150)
    email = models.CharField(max_length=50)
    phone = models.CharField(max_length=50)
    map = models.URLField(null=True)


