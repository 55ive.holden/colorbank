from django.urls import path
from users.views import UserRegisterView, UserEditView, PasswordsChangeView, ShowProfilePageView, EditProfilePageView, CreateProfilePageView


urlpatterns = [
    path('register/', UserRegisterView.as_view(), name='register_url'),
    path('login/', UserRegisterView.as_view(), name='login_url'),
    path('settings_profile/', UserEditView.as_view(), name='settings_profile_url'),
    path('password/', PasswordsChangeView.as_view(template_name='change-password_url')),
    path('profile/<int:pk>/', ShowProfilePageView.as_view(), name='profile_url'),
    path('edit_profile_page/<int:pk>/', EditProfilePageView.as_view(), name='edit_profile_page_url'),
    path('create_profile_page/', CreateProfilePageView.as_view(), name='create_profile_page_url'),
]
