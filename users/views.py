from django.shortcuts import get_object_or_404
from django.views import generic
from django.views.generic import DetailView, CreateView
from users.forms import SignUpForm, SettingsForm, PasswordChangedForm, ProfilePageForm
from django.urls import reverse_lazy
from django.contrib.auth.views import PasswordChangeView
from users.models import Profile


class ShowProfilePageView(DetailView):
    model = Profile
    template_name = 'profiles/profile.html'

    def get_context_data(self, **kwargs):
        context = super(ShowProfilePageView, self).get_context_data(**kwargs)
        page_user = get_object_or_404(Profile, id=self.kwargs['pk'])
        context['page_user'] = page_user
        return context


class UserRegisterView(generic.CreateView):
    form_class = SignUpForm
    template_name = 'registration/register.html'
    success_url = reverse_lazy('login')


class UserEditView(generic.UpdateView):
    form_class = SettingsForm
    template_name = 'registration/settings_profile.html'
    success_url = reverse_lazy('index_url')

    def get_object(self):
        return self.request.user


class PasswordsChangeView(PasswordChangeView):
    form_class = PasswordChangedForm
    success_url = reverse_lazy('index_url')


class EditProfilePageView(generic.UpdateView):
    model = Profile
    form_class = ProfilePageForm
    template_name = 'profiles/edit_profile_page.html'
    success_url = reverse_lazy('index_url')


class CreateProfilePageView(CreateView):
    model = Profile
    form_class = ProfilePageForm
    template_name = 'profiles/create_profile_page.html'

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


