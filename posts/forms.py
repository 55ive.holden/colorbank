from ckeditor.fields import RichTextField
from django import forms
from posts.models import Post, Comment


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ('title', 'image', 'content')

        widgets = {
            'title': forms.TextInput(attrs={'class': 'form_control'}),
            'image': forms.FileInput(),
            'content': forms.Textarea(attrs={'class': 'form_control'}),
        }


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('body',)

        widgets = {
            'body': forms.Textarea(attrs={'class': 'form_control'})
        }
