from django.contrib.auth.models import User
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from posts.models import Post, Comment
from posts.forms import PostForm, CommentForm
from django.urls import reverse_lazy, reverse
from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect


class PostListView(ListView):
    model = Post
    template_name = 'posts/posts_list.html'
    ordering = ['-id']


class PostDetailView(DetailView):
    model = Post
    template_name = 'posts/post_detail.html'

    def get_context_data(self, *args, **kwargs):
        context = super(PostDetailView, self).get_context_data(**kwargs)
        stuff = get_object_or_404(Post, id=self.kwargs['pk'])
        total_likes = stuff.total_likes()
        liked = False
        if stuff.likes.filter(id=self.request.user.id).exists():
            liked = True
        context["total_likes"] = total_likes
        context["liked"] = liked
        return context


class MixinPostView:
    model = Post
    form_class = PostForm
    pk = None

    def form_valid(self, form):
        item = form.save()
        self.pk = item.pk
        return super().form_valid(form)

    def get_object(self, queryset=None):
        obj = self.model.objects.filter(pk=self.kwargs['pk']).first()
        user = self.request.user
        if user.is_authenticated:
            obj.author = User.objects.filter(id=user.id).first()
            obj.save(update_fields=('author',))
        return obj

    def get_success_url(self):
        return reverse('post_detail_url', kwargs={'pk': self.pk})


class AddPostView(MixinPostView, CreateView):
    model = Post
    template_name = 'posts/add_post.html'


class UpdatePostView(MixinPostView, UpdateView):
    model = Post
    template_name = 'posts/update_post.html'


class DeletePostView(DeleteView):
    model = Post
    template_name = 'posts/delete_post.html'
    success_url = reverse_lazy('posts_list_url')


def like_view(request, pk):
    post = get_object_or_404(Post, id=request.POST.get('post_id'))
    liked = False
    if post.likes.filter(id=request.user.id).exists():
        post.likes.remove(request.user)
        liked = False
    else:
        post.likes.add(request.user)
        liked = True
    return HttpResponseRedirect(reverse('post_detail_url', args=[str(pk)]))


class AddCommentView(CreateView):
    model = Comment
    form_class = CommentForm
    template_name = 'posts/add_comment.html'

    def form_valid(self, form):
        form.instance.post_id = self.kwargs['pk']
        form.instance.name = self.request.user.first_name
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('post_detail_url', kwargs={'pk': self.kwargs['pk']})




