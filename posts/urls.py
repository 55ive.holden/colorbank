from django.urls import path
from posts import views

urlpatterns = [
    path('posts/', views.PostListView.as_view(), name='posts_list_url'),
    path('posts/<int:pk>', views.PostDetailView.as_view(), name='post_detail_url'),
    path('posts/add_post/', views.AddPostView.as_view(), name='add_post_url'),
    path('posts/edit/<int:pk>', views.UpdatePostView.as_view(), name='update_post_url'),
    path('posts/<int:pk>/delete', views.DeletePostView.as_view(), name='delete_post_url'),
    path('like/<int:pk>/delete', views.like_view, name='like_post_url'),
    path('posts/<int:pk>/add_comment', views.AddCommentView.as_view(), name='add_comment_url'),
]
