from django.contrib import admin
from gallery.models import Category, Image, ImageComment

admin.site.register(Category)
admin.site.register(Image)
admin.site.register(ImageComment)


