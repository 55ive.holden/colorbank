from django.urls import path
from gallery import views


urlpatterns = [
    path('', views.IndexTemplateView.as_view(), name='index_url'),
    path('categories/', views.CategoryListView.as_view(), name='category_list_url'),
    path('categories/<slug:slug>/', views.CategoryDetailView.as_view(), name='category_detail_url'),
    path('categories/<slug:slug>/<int:pk>/', views.ImageDetailView.as_view(), name='image_detail_url'),
    path('categories/<slug:slug>/<int:pk>/add_comment/', views.ImageCommentView.as_view(), name='image_comment_url'),
    path('categories/<slug:slug>/<int:pk>/like', views.image_like_view, name='like_image_url'),

]
