from django import forms
from gallery.models import ImageComment


class ImageCommentForm(forms.ModelForm):
    class Meta:
        model = ImageComment
        fields = ('body',)

        widgets = {
            'body': forms.Textarea(attrs={'class': 'form_control'})
        }
