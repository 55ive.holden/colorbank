from django.http import HttpResponseRedirect, HttpResponse, request
from django.shortcuts import get_object_or_404, render
from django.views.generic import TemplateView, ListView, DetailView, CreateView
from gallery.models import Category, Image, ImageComment
from django.urls import reverse_lazy, reverse
from gallery.forms import ImageCommentForm


class IndexTemplateView(TemplateView):
    template_name = 'index.html'


class CategoryListView(ListView):
    template_name = 'gallery/category_list.html'
    model = Category


class CategoryDetailView(DetailView):
    template_name = 'gallery/category_detail.html'
    model = Category
    context_object_name = 'category'


class ImageDetailView(DetailView):
    model = Image
    template_name = 'gallery/image_detail.html'

    def get_context_data(self, **kwargs):
        the_context = super(ImageDetailView, self).get_context_data(**kwargs)
        the_stuff = get_object_or_404(Image, id=self.kwargs['pk'])
        total_likes = the_stuff.total_likes()
        liked = False
        if the_stuff.likes.filter(id=self.request.user.id).exists():
            liked = True
        the_context["total_likes"] = total_likes
        the_context["liked"] = liked
        return the_context


class ImageCommentView(CreateView):
    model = ImageComment
    form_class = ImageCommentForm
    template_name = 'gallery/add_comment_image.html'

    def form_valid(self, form):
        form.instance.image_id = self.kwargs['pk']
        form.instance.name = self.request.user.first_name
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('image_detail_url', kwargs={'slug': self.kwargs['slug'], 'pk': self.kwargs['pk']})


def image_like_view(request, slug, pk):
    image = get_object_or_404(Image, id=request.POST.get('image_id'))
    liked = False
    if image.likes.filter(id=request.user.id).exists():
        image.likes.remove(request.user)
        liked = False
    else:
        image.likes.add(request.user)
        liked = True
    return HttpResponseRedirect(reverse('image_detail_url', kwargs={'slug': slug, 'pk': pk}))



