from django.core.files.base import ContentFile
from django.core.management import BaseCommand


from gallery.models import LabelLink


class Command(BaseCommand):
    LABEL_MAP = {
        'vk': {
            'publish': True,
            'icon_path': '/home/mgdas/projects/gallery/static/assets/img/icons/vk.png',
            'link': 'https://vk.com'
        },
        'instagram': {
            'publish': True,
            'icon_path': '/home/mgdas/projects/gallery/static/assets/img/icons/instagram.png',
            'link': 'https://instagram.com'
        },
        'facebook': {
            'publish': True,
            'icon_path': '/home/mgdas/projects/gallery/static/assets/img/icons/facebook.png',
            'link': 'https://facebook.com'
        },
    }

    def handle(self, *args, **options):
        for name, value in self.LABEL_MAP.items():
            label, _ = LabelLink.objects.get_or_create(
                title=name,
                icon='',
                link=value.get('link'),
                publish=value.get('publish')
            )

            with open(value.get('icon_path', ''), 'rb') as file:
                label.icon.save(name, ContentFile(file.read()), save=True)

