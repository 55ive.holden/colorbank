from ckeditor.fields import RichTextField
from django.contrib.auth.models import User
from django.db import models


class AbstractDateModel(models.Model):
    class Meta:
        abstract = True

    created = models.DateTimeField(auto_now=True)


class AbstractBaseModel(AbstractDateModel):
    class Meta:
        abstract = True

    title = models.CharField(max_length=100, db_index=True)
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.title


class Category(AbstractBaseModel):
    image = models.FileField(upload_to='category')
    slug = models.SlugField(max_length=70, null=True, blank=True)


class Image(AbstractBaseModel):
    image = models.FileField(upload_to='images')
    category = models.ForeignKey(Category, on_delete=models.PROTECT, related_name='images')
    likes = models.ManyToManyField(User, related_name='image_likes')

    def total_likes(self):
        return self.likes.count()


class ImageComment(models.Model):
    image = models.ForeignKey(Image, related_name='image_comment', on_delete=models.CASCADE, null=True, blank=True)
    name = models.CharField(max_length=100, db_index=True)
    body = RichTextField(blank=True, null=True)
    date_added = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%s - %s' % (self.image, self.name)


